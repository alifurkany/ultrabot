require("dotenv").config();
const config = require("./config.json");
const discord = require("discord.js");

const client = new discord.Client({
	intents: [
		"Guilds",
		"GuildMembers",
		"GuildMessages",
		"GuildMessageReactions",
	],
});

const commands = [
	new discord.SlashCommandBuilder()
		.setName("bewnewnewnewnewnew")
		.setDescription("bad to the bone"),
	new discord.SlashCommandBuilder()
		.setName("color")
		.setDescription("Set your role color")
		.addStringOption((o) =>
			o
				.setName("hex")
				.setDescription("Set the color you want in HEX RGB format")
				.setMaxLength(6)
				.setMinLength(6)
				.setAutocomplete(false)
				.setRequired(true)
		),
	new discord.SlashCommandBuilder()
		.setName("removecolor")
		.setDescription("Remove your color role"),
];

client.once("ready", async () => {
	const rest = new discord.REST({ version: "10" }).setToken(
		process.env.TOKEN
	);
	try {
		await rest.put(
			discord.Routes.applicationCommands(process.env.CLIENT_ID),
			{
				body: commands,
			}
		);
		console.log("Registered commands");
	} catch (error) {
		console.error(error);
	}
});

client.on("ready", async () => {
	console.log("Fetching reaction role messages");
	await Promise.all(
		config.reactions.map((x) =>
			client.channels
				.fetch(x.channelId)
				.then((y) => y.messages.fetch(x.messageId))
		)
	);
	console.log("Fetched reaction role messages");
});

client.on("messageReactionAdd", async (reaction, user) => {
	let reactionMessage = config.reactions.find(
		(v) => v.messageId === reaction.message.id
	);
	if (!reactionMessage) return;
	let roleId = reactionMessage.roles[reaction.emoji.identifier];
	if (!roleId) return;
	let member = await reaction.message.guild.members.fetch(user.id);
	if (member.roles.cache.some((r) => r.id === roleId)) {
		await member.roles.remove(roleId);
		let reply = await reaction.message.channel.send(
			`Removed a role from <@${user.id}>`
		);
		setTimeout(() => reply.delete(), 2000);
	} else {
		await member.roles.add(roleId);
		let reply = await reaction.message.channel.send(
			`Gave a role to <@${user.id}>`
		);
		setTimeout(() => reply.delete(), 2000);
	}
});

client.on("interactionCreate", async (interaction) => {
	if (!interaction.isChatInputCommand()) return;
	switch (interaction.commandName) {
		case "bewnewnewnewnewnew":
			await interaction.reply(
				"https://www.youtube.com/watch?v=TFwXbp9bLlY"
			);
			break;
		case "color": {
			let hex = interaction.options.getString("hex");
			if (!hex.match(/^[0-9a-f]{6}$/i)) {
				await interaction.reply({
					ephemeral: true,
					content: "That's not a valid 6-digit hexadecimal number",
				});
				break;
			}
			await interaction.deferReply({ ephemeral: true });
			let roles = await interaction.guild.roles.fetch();
			let member = await interaction.guild.members.fetch(
				interaction.user.id
			);
			let role = member.roles.cache.find((r) =>
				r.name.match(/^#[0-9a-f]{6}$/)
			);
			if (!role) {
				let newRole = await interaction.guild.roles.create({
					name: `#${hex}`,
					color: `#${hex}`,
					position: roles.get(config.roleSeparator).position - 1,
				});
				await member.roles.add(newRole.id);
				await interaction.editReply({
					content: "Gave you a role color",
				});
			} else {
				await role.edit({ color: `#${hex}` });
				await interaction.editReply({
					content: "Changed your role color",
				});
			}
			break;
		}
		case "removecolor": {
			await interaction.deferReply({ ephemeral: true });
			let member = await interaction.guild.members.fetch(
				interaction.user.id
			);
			let role = member.roles.cache.find((r) =>
				r.name.match(/^#[0-9a-f]{6}$/)
			);
			if (!role) {
				await interaction.editReply({
					content: "You don't have a color role",
				});
				break;
			}
			await member.roles.remove(role.id);
			await interaction.editReply({
				content: "Removed your color role",
			});
			break;
		}
	}
});

client.login(process.env.TOKEN);
